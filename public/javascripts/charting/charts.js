function _drawMainChart(source,startTime) {
	if (source.length < 1) {
		showNoInfo()
	} else {
		var data = __extractData(source,startTime)
		if(isEmpty(data)){
			showNoInfo()
			return
		}else{
			$('#chartBlock').show('slow', function() {
				$('#chartNode').show()
				$('#chartNode').empty();
				draw(data, 'chartNode',startTime)
			})
		}
	}

}
function showNoInfo(){
	$("#canvasDiv").fadeOut(300, function() {
		$("#chartBlock").slideUp(400, function() {
			$("#noInfo").slideDown('fast')
			$("#totalTime").slideUp('fast')
		})
	})
}
function isEmpty(data){
	for (var i=0;i<24;i++){
		if(data.hasOwnProperty(i+"")){
			return false;
		}
	}
	return true;
}
function draw(data, chartName,startTime) {
	var series = _compress(data)
	var wasted="Бездарно потрачено времени: "
	var minutes=series.data.reduce(function(prev,curr){
		return prev+curr
	})
	var totalString=wasted
	
	var time = _getTime(minutes)
	if(time.hours != 0){
		totalString+=" "+time.hours+"ч"
	}
	if(time.minutes != 0){
		totalString+=" "+time.minutes+"мин"
	}
	
	var error = series.error
	var errorTime = _getTime(error)
	if(errorTime.hours != 0 || errorTime.minutes!=0){
		totalString+=" (возможная погрешность:"
		if(errorTime.hours != 0){
			totalString+=" "+errorTime.hours+"ч"
		}
		if(errorTime.minutes != 0){
			totalString+=" "+errorTime.minutes+"мин)"
		}else{
			totalString+=")"
		}
	}
	
	$("#totalTime").text(totalString)
	$("#totalTime").show()
	_doDraw(chartName, series.data)
}

function _getTime(minutes){
	var result={"hours":0,"minutes":0}
	if(Math.floor(minutes/60)!=0){
		result.hours=Math.floor(minutes/60)
	}
	if(minutes%60!=0){
		result.minutes=minutes%60
	}
	return result
}

function _compress(input) {
	var result = []
	var value;
	var newItem;
	var totalError = 0
	for ( var i = 0; i < 24; i++) {
		newItem = 0;
		value = _findItem(input, i)
		if (value != null) {
			for ( var j = 0; j < value.length; j++) {
				newItem += (value[j][1] - value[j][0])
				totalError += _calculateError(value[j])
			}
		}
		result.push(newItem)
	}
	return {"data":result,"error":totalError}
}

//TODO: REWRITE THAT SHIT
//выходной формат {10:[[15,30],[35,45]],11:[blabla]}
function __extractData(data,startTime) {
	var newRes = new Object()
	var i = 0
		
	while (i<data.length && data[i]["online"] == 0)
		i++
		
	var len = data.length
	var len = data.length-1
	while (len >= 0 && len >= i) {
		if(new Date(data[len]["time"]) <= new Date(startTime) ||
				new Date(data[len]["time"]) >= new Date(startTime+86400000)){
			data.splice(len)
		}
		len--
	}
		
	if(i==data.length){
		return newRes
	}
	var isOnline = true
	var _in = 0
	var _out = 0
	do {
		var pair = _mergeTime(data, i, isOnline)
		i = pair["index"]
		_in = pair["time"]
		isOnline = !isOnline
		if (i == data.length) {
			var tempOut = new Date()//endOfDay
			if(tempOut.getDate()==_in.getDate()){
				_out=tempOut
			}else{
				break;
			}
//			if(tempOut.getHours()!=_in.getHours()){
//				//making it up to a full hour
//				_out=new Date(_in.getTime()+(60-_in.getMinutes())*60*1000);
//			}
		} else {
			var pair = _mergeTime(data, i, isOnline)
			i = pair["index"]
			_out = pair["time"]
		}
		isOnline = !isOnline
		_put(newRes, _in, _out)

		_in = 0
		_out = 0

	} while (i < data.length)

	return newRes

}

//сливаем последовательность 1 1 1 в одно число
function _mergeTime(data, index, isOnline) {
	var times = []
	var time = new Number()

	while (index != data.length && data[index]["online"] == isOnline) {
		times.push(data[index++]["time"])
	}
	time = times.reduce(function(prev, curr) {
		return new Number(prev) + new Number(curr);
	})
	return {
		'index' : index,
		'time' : new Date(time / times.length)
	}
}

function _calculateError(sector){
	var value = 0
	if(sector[1]==60 && sector[0] == 0){
		value = 0
	}else{
		if(sector[1]-sector[0] > 10){
			value=10
		}else{
			if(sector[1] == 60){
				if(sector[1]-sector[0] > 5){
					value = sector[1]-(sector[0]+5)
				}
			}else if(sector[0] == 0){
				value = sector[1]-sector[0]
			}
		}
	}
	
	return value
}


function _findItem(data, hour) {
	for ( var item in data) {
		if (item == hour) {
			return data[item]
		}
	}
	return null;
}

function _put(target, _in, _out) {
	var diff = _out.getHours() - _in.getHours();
	var existingItem = _findItem(target, _in.getHours());
	if (existingItem == null) {
		if (diff == 0) {
			target[_in.getHours()] = [ [ _in.getMinutes(), _out.getMinutes() ] ]
		} else {
			// TODO WHAT THE FUCK IS THIS
			target[_in.getHours()] = [ [ _in.getMinutes(), 60 ] ]
			_fillItem(_in, _out, diff, target);

		}

	} else {
		if (diff == 0) {
			existingItem.push([ _in.getMinutes(), _out.getMinutes() ])
		} else {
			// existingItem["minute"] += 60 - _in.getMinutes()
			existingItem.push([ _in.getMinutes(), 60 ]);
			// TODO DRY, jeez
			_fillItem(_in, _out, diff, target);
		}
	}
	
	function _fillItem(_in, _out, diff, item) {
		for ( var k = 1; k < diff; k++) {
			item[_in.getHours() + k] = [ [ 0, 60 ] ]
		}
		// FIXME
		if (_out.getMinutes() != 0)
			item[_out.getHours()] = [ [ 0, _out.getMinutes() ] ]
	}

}

function _doDraw(chartName, series) {
	plot = $.jqplot(chartName, [ series ], {
		captureRightClick : true,
		seriesDefaults : {
			renderer : $.jqplot.BarRenderer,
			rendererOptions : {
				barMargin : 3,
				highlightMouseDown : true
			},
			pointLabels : {
				show : false
			}
		},
		axes : {
			xaxis : {
				renderer : $.jqplot.CategoryAxisRenderer,
				min : 0,
				ticks : (function(begin, end) {
					out = [];
					for ( var i = begin, idx = 0; i < end; i++, idx++) {
						out[idx] = i;
					}
					return out;
				}(0, 24))
			},
			yaxis : {
				min : 0,
				max : 60

			}
		}

	});
}

function _toAngle(minute) {
	var k = -(90- 6 * minute )
	var degree = k < 0 ? 360 + k : k
	return degree * (Math.PI / 180);
}
function _toAngle2(minute) {
	var k = (90- 6 * minute)
	var degree = k < 0 ? 360 + k : k
	return degree * (Math.PI / 180);
}

function _drawSector(g, circle, sector, fill, style) {
	var x = circle.x
	var y = circle.y
	var radius = circle.radius
	g.save()

	g.beginPath();
	var lineEndX;
	var lineEndY;
	g.moveTo(x, y);
	lineEndX = x + Math.cos(_toAngle2(sector[0])) * radius;
	lineEndY = y - Math.sin(_toAngle2(sector[0])) * radius;
	g.lineTo(lineEndX, lineEndY)

	if (sector[0] == 0 && sector[1] == 60) {
		_drawCircle(g, x, y, radius)
	} else {
		g.arc(x, y, radius, _toAngle(sector[0]), _toAngle(sector[1]), false);
	}

	g.moveTo(x,y)
	lineEndX = x + Math.cos(_toAngle2(sector[1])) * radius;
	lineEndY = y - Math.sin(_toAngle2(sector[1])) * radius;
	g.lineTo(lineEndX,lineEndY)
	
	g.closePath()
	if (fill) {
		g.fillStyle = style.color.getRGBA()
		g.fill();
	} else {
		g.strokeStyle = style.color.getRGBA()
		g.lineWidth = style.lineWidth
		g.stroke();
	}

	g.restore()
}


function _drawString(g, string, x, y, style, font) {
	g.save()
	if(font){
		g.font = font;
	}
	g.fillStyle = style.color.getRGBA()
	g.fillText(string, x, y);
	g.restore()
}
function _drawCircle(g,x,y,radius){
	g.save()
	g.beginPath();
	g.arc(x, y, radius, Math.PI*2, 0, true);
	g.closePath();
	g.stroke();
	g.restore()
}
function _width(string){
	var ruler = document.getElementById("ruler");
    ruler.innerHTML = string;
    return ruler.offsetWidth;
}
function _height(string){
	var ruler = document.getElementById("ruler");
    ruler.innerHTML = string;
    return ruler.offsetHeight;
}
function __insideCircle(x,y,circle){
	var xdiff=(x-circle.x)
	var ydiff=(y-circle.y)

	return Math.sqrt(Math.pow(xdiff,2)+Math.pow(ydiff,2))<=circle.radius
}

function _getQuarter(x,y,circle){
	if(x>=circle.x){
		if(y>=circle.y){
			return 4;
		}else{
			return 1;
		}
	}else{
		if(y>=circle.y){
			return 3;
		}else{
			return 2;
		}
	}
	
}
function _getCoords(time,circle){
	var cx = circle.x
	var cy = circle.y
	var radius = circle.radius
	
	var lbX=cx + Math.cos(_toAngle2(time)) * radius;
	var lbY=cy - Math.sin(_toAngle2(time)) * radius;
	var quarter=_getQuarter(lbX, lbY, circle);
	var result={
		'x':lbX,
		'y':lbY
	}
	var threshold=3
	var stringWidth=_width(time.toString())
	if(quarter==1){
		if(time==60){
			result.x-=stringWidth
		}
		result.x+=threshold
		result.y-=threshold
	}else if(quarter==2){
		if(__insideCircle(lbX+stringWidth,lbY,circle)){
			result.x-=stringWidth+threshold
		}
	}else if(quarter==3){
		if(__insideCircle(lbX+stringWidth,lbY-stringWidth,circle)){
			result.x-=stringWidth
			result.y+=stringWidth
		}
	}else if(quarter==4){
		if(__insideCircle(lbX,lbY-stringWidth,circle)){
			result.y+=stringWidth
		}
	}
	return result
}

function _drawTimeStamps(g, circle, sector, style, font){
	var cx = circle.x
	var cy = circle.y
	var radius = circle.radius
	g.save()
	if (sector[0] == 0 && sector[1] == 60) {
		var failImage=new Image()
		failImage.src = '/public/images/fail.png';
		failImage.onload = function() {
			g.save()
			g.drawImage(failImage, 25, cy-radius, radius*2, radius*2);
			g.restore()
			
		}
	}else{
		var coords=_getCoords(sector[0], circle);
		_drawString(g, sector[0], coords.x, coords.y,style,font)
	
		coords=_getCoords(sector[1], circle);
		_drawString(g, sector[1], coords.x, coords.y,style,font)
	}
	g.restore()

}
function drawPie(source, hour, g, circle) {
	var fill=true
	var allFillStyle= {
		'color':new Color("rgba(128,182,229,170)")
	}
	var okfillStyle= {
		'color':new Color("rgba(196,255,145,170)")
	}
	var font="bold 24px sans-serif"	
	
	var strokeStyle={
		'color':new Color("rgba(128,128,128,255)"),
		'lineWidth':3	
	};
	var numberStyle={
		'color':new Color("rgba(237,130,79,255)")
	}
	
	var clockImage = new Image();
	var x = circle.x
	var y = circle.y
	var radius = circle.radius
	g.strokeStyle = strokeStyle.color.getRGBA()
	g.lineWidth = strokeStyle.lineWidth
	
	clockImage.src = '/public/images/clock2.png';
	clockImage.onload = function() {
		g.save()
		source.forEach(function(sector) {
			_drawSector(g, circle, sector, !fill, strokeStyle)
			_drawSector(g, circle, sector, fill, allFillStyle)
			var okSector
			var value = 0
			
			if(sector[1]==60 && sector[0]==0){
				okSector=[sector[0],sector[1]]
			}else{
				if(sector[1]-sector[0] > 10){
					okSector=[sector[0],sector[1]-10]
				}else{
					if(sector[1] == 60){
						if(sector[1]-sector[0] > 5){
							okSector=[sector[0],sector[0]+5]
						}else{
							okSector=[sector[0],sector[1]]
						}
					}else if(sector[0] == 0){
						return
					}else{
						okSector=[sector[0],sector[1]]
					}
				}
			}
			_drawSector(g, circle, okSector, fill, okfillStyle)
		});
		g.drawImage(clockImage, x-radius, y-radius, radius*2, radius*2);
		_drawCircle(g, x, y, radius)
		source.forEach(function(sector) {
			_drawTimeStamps(g, circle, sector, numberStyle, "bold 18px sans-serif")
		});
		g.restore()
	}
	var string = hour + ".00 - " + (hour + 1) + ".00";
	_drawString(g, string, (270 - string.length * 12) / 2, 50, allFillStyle, font)
}