var isExtended=false;
var chartNode
var canvas
var canvasDiv
var circle={
	x:130,
	y:200,
	radius:105
}
var sourceData;
var bottomLayer
var currentHour
var currentDate
// FIXME WHAT THE FUCK IS THAT
var hoursDiff;
var dateInMillis;
var dayDiff=24*60*60*1000;

function _restore(){
	canvasDiv.fadeOut(300)
	$("#totalTime").fadeOut(400)
	chartNode.fadeOut(400,function(){
		chartNode.width(940)
		chartNode.empty();
		_getDataAndDrawMainChart()
	})
	isExtended=false;
}
function getTimeDiff(callback) {
    $.ajax({
        url : '/time',
        dataType : "text",
        success : function(data, textStatus) {
            var now = new Date();
            var serverNow = new Date(new Number(data));
            hoursDiff = serverNow.getHours() - now.getHours();
            callback()
        }
    });
}

function getDateFromMillis(millis) {
    return new Date(new Number(millis));
}
function _getDataAndDrawMainChart(){
    dateInMillis = currentDate + (hoursDiff * 3600000);
    if(isExtended){
    	console.log("")
    	chartNode.children().fadeOut(250).promise().then(function() {
    		chartNode.empty();
    		getData(_drawMainChart);
    		chartNode.children().fadeIn(500);
    	});
    }else{
    	getData(_drawMainChart);
    }
}

function getData(callback){
	_getData($("#uid").html(), dateInMillis,
            dateInMillis + 86400000,callback);
}
// TODO: REWRITE
function _getData(uid, start, end,callback){
	var varan
	 $.ajax({
         url : "/getTargetInfo?uid=" + uid + "&start=" + start
         + "&end=" + end,
         dataType : "json",
         timeout : 5000,
         beforeSend : function(){
        	 varan = new Date()
        	 $("#loadingImage").show();
         },
         complete : function(){
        	 console.log(new Date()-varan)
        	 $("#loadingImage").hide();
         },
         success : function(data, textStatus) {
        	 console.log("GETDATA")
             try {
                 for ( var i in data) {
                     console.log(data[i]["uid"]
                         + " "
                         + data[i]["online"]
                         + " "
                         + new Date(new Number(data[i]["time"]))
                         .toLocaleString());
                 }
						
             } catch (e) {
                 alert(e);
             }
             sourceData=data;
             if(callback!=undefined){
            	 callback(sourceData,start)
             }
         },
         error : function(xhr,status,errorThrown){
        	 if(status=="timeout"){
        		sourceData = []
        		$("#requestTimedOut").slideDown(400)
        		canvasDiv.fadeOut(300)
				$("#totalTime").fadeOut(400)
				$("#canvasDiv").fadeOut(300, function() {
					$("#chartBlock").slideUp(400, function() {
						$("#totalTime").slideUp('fast')
					})
				})
        	 }
         }
         
     });
}

function main() {
	currentDate=new Date().getTime()
	$("#noInfo").hide()
	$("#requestTimedOut").hide()
	chartNode=$('#chartNode')
	canvas = document.getElementById("canvas");
	canvasDiv=$("#canvasDiv")
	bottomLayer = canvas.getContext('2d');
	
	chartNode.bind('jqplotDataClick', barClickListener);
    $(function() {
    	$("#datepicker").datepicker();
    });

    $("#extraInfo").hide();
    
    var now = new Date();
    var day=now.getDate()<10?"0"+now.getDate():now.getDate()
    var month=now.getMonth()+1
    var monthString=month<10?"0"+month:month
    var defaultDate = day + '-' + monthString + '-' + now.getFullYear();
    $("#datepicker").val(defaultDate);
    
	$("#datepicker").datepicker({
		firstDay : 1,
		dayNamesMin : [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
        dateFormat : "dd-mm-yy",
        showAnim: "slideDown",
        defaultDate: "0",
        onSelect : function(){
        	processDateSelection();
        }
    });
	
    $("#leftButton").click(function(){
    	$("#noInfo").slideUp('fast')
    	$("#requestTimedOut").slideUp('fast')
    	if(dayDiff>0){
    		dayDiff*=-1;
    	}
    	processNavigationClick();
    })
    
    $("#rightButton").click(function(){
    	$("#noInfo").slideUp('fast')
    	$("#requestTimedOut").slideUp('fast')
    	if(dayDiff<0){
    		dayDiff*=-1;
    	}
    	processNavigationClick();
   	})
    
    $("#showExtraInfo").click(function(){
    	$("#extraInfo").slideToggle('fast')
    })
    
    getTimeDiff(processDateSelection);
    
    $("#reloadButton").click(function() {
    	processDateSelection();
    });
}

function processDateSelection(){
	$("#noInfo").slideUp('fast')
	$("#requestTimedOut").slideUp('fast')
	currentDate = $("#datepicker").datepicker("getDate").getTime();
	if (currentDate == null) {
		alert("Выберите день!");
		return;
	}
	$("#requestTimedOut").slideUp(400)
//	_getDataAndDrawMainChart()
	$("#requestTimedOut").slideUp(400)
	setTimeout(function(){
		_restore();
	},500)
}

function processNavigationClick(){
	var selected=$("#datepicker").datepicker("getDate");
	if(selected!=null){
		currentDate=new Date(selected).getTime()
	}
	currentDate+=dayDiff
	$('#datepicker').datepicker("setDate", new Date(currentDate));
	$("#requestTimedOut").slideUp(400)
	setTimeout(function(){
		_restore();
	},500)
}
function barClickListener(ev, seriesIndex, pointIndex, data) {
   	console.log('series: '+seriesIndex+', point: '+pointIndex+', data: '+data)
   	currentHour=pointIndex
	if (canvas.getContext) {
		var sectors=_findItem(__extractData(sourceData),pointIndex)
        if(!isExtended){
           	isExtended=true;
           	chartNode.children().fadeOut(500).promise().then(function() {
           		chartNode.empty();
           		chartNode.width(680)
           		chartNode.children().fadeIn(500).promise().then(function(){
           			_drawMainChart(sourceData)
           			canvasDiv.width(260)
               		canvasDiv.fadeIn(350)
               		bottomLayer = canvas.getContext('2d');
               	})
           	});
        }
		bottomLayer.clearRect(0,0,canvas.width,canvas.height)
        drawPie(sectors,currentHour,bottomLayer,circle)
	} else {
		console.log(canvas.getContext)
		alert("canvas isn't working lol");
	}
}

function _insideCircle(ev){
	var _x;
	var _y;
	if (ev.layerX || ev.layerX == 0) { // Firefox
	    _x = ev.layerX;
	    _y = ev.layerY;
	} else if (ev.offsetX || ev.offsetX == 0) { // Opera
		_x = ev.offsetX;
		_y = ev.offsetY;
	}
	
	var xdiff=(_x-circle.x)
	var ydiff=(_y-circle.y)

	return Math.sqrt(Math.pow(xdiff,2)+Math.pow(ydiff,2))<=circle.radius
}
