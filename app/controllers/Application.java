package controllers;

import static util.StaticDataHolder.dbm;
import static util.StaticDataHolder.lsHolder;
import static util.StaticDataHolder.dbw;
import static util.VkMotitorConstants.URL_BEGINING;
import static util.VkMotitorConstants.URL_ENDING;
import jobs.JobInvoker;
import play.Logger;
import play.Play;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Controller;
import util.ServerStatus;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class Application extends Controller {

	public static final JobInvoker INVOKER = new JobInvoker();

	public static void index() {
		render();
	}

	public static void why() {
		render();
	}

	public static void aboutus() {
		render();
	}
	
	/** Хак, чтобы получить нормальный урл */ 
	public static void show1(String targetId) {
		show(targetId);
	}
	
	public static void show(String targetId) {
		try {
			String[] pieces = targetId.split("vk\\.com\\/");
			targetId = pieces[pieces.length-1];
			ConciseStats stats = getConciseStats(targetId);
			if (stats == null) {
				invalidid();
			}

			String uid = stats.getUserId();
			String online = stats.getStatus();

			if (lsHolder.getStatus(uid) == null) {
				addNewTarget(uid, online, targetId);
			}
			
			render(stats,targetId);

		} catch (Exception e) {
			invalidid();
		}
	}

	private static void addNewTarget(String uid, String online, String targetId) {
		dbw.saveNewRecord(uid, online,
				String.valueOf(System.currentTimeMillis()));
		lsHolder.addNew(uid, online);
		targetAdded(targetId);
	}

	public static ConciseStats getConciseStats(String name) {
		StringBuilder sb = new StringBuilder(URL_BEGINING);
		sb.append(name);
		sb.append(URL_ENDING);
		try {
			WSRequest request = play.libs.WS.url(sb.toString());
			request.timeout("10s");
			HttpResponse resp = request.get();
			if (resp.getStatus() != 200) {
				return null;
			}
			
			JsonElement json = new JsonParser().parse(resp.getString());
			JsonArray arr = json.getAsJsonObject().get("response")
					.getAsJsonArray();
			String online = arr.get(0).getAsJsonObject().get("online")
					.getAsString();
			String firstName = arr.get(0).getAsJsonObject().get("first_name").getAsString();
			String uid = arr.get(0).getAsJsonObject().get("uid").getAsString();
			String lastName = arr.get(0).getAsJsonObject().get("last_name").getAsString();
			String nickName = arr.get(0).getAsJsonObject().get("nickname").getAsString();

			return new ConciseStats(uid,online,firstName,lastName,nickName);

		} catch (Exception e) {
			Logger.error(e, "error while getting concise stats");
			return null;
		}
	}

	public static void targetAdded(String targetId) {
		render(targetId);
	}

	public static void invalidid() {
		render();
	}

	public static void admin_login(String admin) {
		if (admin == null || admin.trim().isEmpty()) {
			error();
		}
		render(admin);
	}

	// POST
	public static void admin_page(String admin, String password) {
		session.put("admin_login", admin);
		session.put("admin_passw", MD5(password));
		checkAdmin();
		render();
	}

	private static void checkAdmin() {
		String adminLogin = session.get("admin_login");
		String passw = session.get("admin_passw");
		if (adminLogin == null || passw == null) {
			error();
		}

		DBCursor dbcur = dbm.security.find(new BasicDBObject().append(
				"admin_login", adminLogin));

		if (dbcur.count() == 0) {
			error();
		}

		try {
			for (DBObject dbo : dbcur) {
				String passwHash = dbo.get("admin_passw").toString();
				if (passw.equals(passwHash)) {
					return;
				}
			}
		} finally {
			dbcur.close();
		}

		error();
	}

	// POST
	public static void _setClientMode() {
		checkAdmin();
		Logger.info("Client Mode was set from admin page");
		dbm.setServerStatus(ServerStatus.STOPPED,
				"client_mode set from admin page");
		INVOKER.stop();
		renderText("DONE!");
	}

	public static void _setServerMode() {
		checkAdmin();
		Logger.info("Server mode was set from admin page");
		dbm.setServerStatus(ServerStatus.STARTED,
				"server_mode set from admin page");
		INVOKER.start();
		renderText("DONE!");
	}

	// POST
	public static void _stopServer() {
		checkAdmin();
		dbm.setServerStatus(ServerStatus.STOPPED, "stopped from admin page");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Logger.info("Stopping Server from admin page");
		Play.stop();
		System.exit(0);
	}

	public static void _logoff() {
		session.clear();
		ok();
	}

	private static String MD5(String md5) {
		if (md5 == null) {
			return null;
		}

		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
}