package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import dbstuff.DbManager;

import jobs.JobInvoker;
import play.Logger;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Controller;
import static util.StaticDataHolder.*;
import static util.VkMotitorConstants.URL_BEGINING;
import static util.VkMotitorConstants.URL_ENDING;

public class AjaxController extends Controller {

	public static void time() {
		renderText(System.currentTimeMillis());
	}

	public static void getFirstTargetRecordTime(String uid) {
		DBCursor cur = dbm.records.find(new BasicDBObject().append("uid", uid))
				.sort(new BasicDBObject("time", new Integer(1))).limit(1);
		if (cur.hasNext()) {
			String s = String.valueOf(cur.next().get("time"));
			cur.close();
			renderText(s);
		} else {
			cur.close();
			renderText("");
		}
	}

	/**
	 * Возвращает 0 или 1 или ошибку
	 * 
	 * @param name
	 *            идентификатор(не обязательно uid) юзера
	 */
	public static void isUserOnline(String name) {

		if (name == null || name.trim().isEmpty()) {
			renderText("name is null or empty");
		}

		try {
			ConciseStats stats = Application.getConciseStats(name);
			if (stats == null) {
				renderText("error.");
			}

			renderText(stats.getStatus());

		} catch (Exception e) {
			renderText("error.");
		}
	}

	public static void getConciseStats(String name) {

		if (name == null || name.trim().isEmpty()) {
			renderText("name is null or empty");
		}

		try {
			ConciseStats stats = Application.getConciseStats(name);
			if (stats == null) {
				renderText("error.");

			}
			String json = new Gson().toJson(stats);
			renderText(json);

		} catch (Exception e) {
			renderText("error.");
		}

	}

	public static void getTargetInfo(String uid, String start, String end) {
		DBCursor cur = null;
		try {
			Long st = Long.parseLong(start);
			Long en = Long.parseLong(end);

			if (st >= en) {
				renderJSON(JSON.serialize(Collections.singletonMap("err",
						"invalid params")));
			}

			cur = dbm.records.find(new BasicDBObject().append("uid", uid)
					.append("time",
							new BasicDBObject().append("$gt", start).append(
									"$lt", end)));

			List<Map> jsonArr = new ArrayList<Map>();
			while (cur.hasNext()) {
				DBObject obj = cur.next();
				jsonArr.add(obj.toMap());

			}
			cur.close();
			
			renderJSON(JSON.serialize(jsonArr));
			
		} catch (Exception e) {
			Logger.error(e, "Error in getTargetInfo(..)");
			dbLogger.log("Error in getTargetInfo(..)", e);
			if (cur != null) {
				cur.close();
			}
			error();
		}
	}
}
