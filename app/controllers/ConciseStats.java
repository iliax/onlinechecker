package controllers;

public class ConciseStats {

	private final String userId;
	private final String status;
	private final String firstName;
	private final String nickName;
	private final String lastName;
	
	public ConciseStats(String userId, String status, String firstName,
			String lastName, String nickName) {
		this.userId = userId;
		this.status = status;
		this.firstName = firstName;
		this.nickName = nickName;
		this.lastName = lastName;
	}
	
	public String getNickName() {
		return nickName;
	}

	public String getUserId() {
		return userId;
	}

	public String getStatus() {
		return status;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	

}
