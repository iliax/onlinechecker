package util;

public class VkMotitorConstants {

	public static final String MONGO_URL_AND_PORT = "ec2-75-101-222-80.compute-1.amazonaws.com:27017";
	
	public static final String DB_NAME = "Online_Checker_DB";

	public static final int THREADS_COUNT = 1;

	public static final String URL_BEGINING = "https://api.vkontakte.ru/method/getProfiles?uids=";
	
	public static final String URL_ENDING = "&fields=online,nickname";
	
	public static final long CHECK_INTERVAL_IN_MILLS = 60 * 1000; // 1 minute
	
	public static final String VK_REQUEST_TIMEOUT = "20s";
	
	public static final int ITEMS_IN_ONE_REQUEST = 150;
}
