package util;

import dbstuff.LastStatusesHolder;
import dbstuff.DbLogger;
import dbstuff.DbManager;
import dbstuff.DbWriter;
import play.Logger;
import play.Play;

public class StaticDataHolder {

	public static volatile DbManager dbm;

	static {
		try {
			dbm = new DbManager();
		} catch (Exception e) {
			Logger.error(" mongo MongoException", e);
			Play.stop();
		}
	}

	public static volatile DbLogger dbLogger = new DbLogger(dbm);

	public static volatile LastStatusesHolder lsHolder = new LastStatusesHolder(dbm);
	
	public static volatile DbWriter dbw = new DbWriter(dbm);
}
