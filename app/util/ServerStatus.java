package util;

public enum ServerStatus {

	STARTED("1"), STOPPED("0");
	
	private String value; 
	
	private ServerStatus(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
