package jobs;

import static util.VkMotitorConstants.*;
import java.util.ArrayList;
import java.util.List;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import controllers.Application;

import play.Logger;
import play.Play;
import play.jobs.Job;
import play.jobs.JobsPlugin;
import play.jobs.OnApplicationStart;
import util.ServerStatus;
import util.StaticDataHolder;
import static jobs.JobInvoker.*;
import static util.StaticDataHolder.*;

@OnApplicationStart
public class Init extends Job {

	private static final String SERVER = "server";
	private static final String CLIENT = "client";

	@Override
	public void run() {

		setAppMode();

		try {
			// чтобы сначала инициировались статические поля
			Class.forName(StaticDataHolder.class.getName());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}

		initCache();
	}

	/** Нагреваем кеш */
	private void initCache() {
		try {
			Logger.info("--- initializing cache... ---");

			lsHolder.initCache();

			Logger.info("--- cache initialized ! ---");
		} catch (Exception e) {
			Logger.error(e, "Initailizing cache error", new Object[0]);
			Play.stop();
		}
	}

	private void setAppMode() {

		String startMode = Play.configuration
				.getProperty("application.run_mode");

		if (startMode == null || startMode.equals(CLIENT)) {
			Logger.info("----- running in client mode -------");
		} else {
			if (startMode.equals(SERVER)) {
				Logger.info("----- running in server mode -------");
			} else {
				Logger.info("----- invalid parameter -------");
				Logger.info("----- running in server mode anyway -------");
			}
			dbm.setServerStatus(ServerStatus.STARTED, "Application started");
			Application.INVOKER.start();
		}

	}

}
