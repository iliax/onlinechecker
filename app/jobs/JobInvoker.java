package jobs;

import static util.VkMotitorConstants.*;
import static util.StaticDataHolder.*;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;



import org.bson.BSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;
import com.sun.java.swing.plaf.windows.WindowsBorders.DashedBorder;

import dbstuff.DbLogger.LEVEL;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.JobsPlugin;
import play.jobs.On;
import play.jobs.OnApplicationStart;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import static util.VkMotitorConstants.*;

public class JobInvoker extends Job {

	public static final int INTERVAL=60;

	private static List<String> getTargetsList() {
		return lsHolder.getTargetUids();
	}
	
	public void stop(){
		JobsPlugin.executor.remove(this);
	}
	public void start(){
		every(INTERVAL);
	}

	@Override
	public void doJob() throws Exception {

		if (JobRunnable.count.get() < THREADS_COUNT) {
			dbw.addCheckLogRecord("passed(prev iteration !processed)");
			return;
		}

		//Logger.info("\n -- started at " + new Date().toString());

		List<String> targets = getTargetsList();

		int part = targets.size() / THREADS_COUNT;

		Thread th = new Thread(new JobRunnable(targets.subList(0, part)));
		th.setDaemon(true);
		th.start();

//		th = new Thread(new JobRunnable(targets.subList(part, part * 2)));
//		th.setDaemon(true);
//		th.start();
//
//		th = new Thread(
//				new JobRunnable(targets.subList((part * 2), (part * 3))));
//		th.setDaemon(true);
//		th.start();
//
//		th = new Thread(new JobRunnable(targets.subList((part * 3),
//				targets.size())));
//		th.setDaemon(true);
//		th.start();

	}

	// util
	@Deprecated
	public static void addOnlineTargetsAfterId(int id, int count) {

		for (int j = 0; j < count;) {
			StringBuilder sb = new StringBuilder(URL_BEGINING);

			for (int ii = 0; ii < 100; ii++) {
				sb.append("id" + id++);
				sb.append(",");
			}

			sb.deleteCharAt(sb.length() - 1);
			sb.append(URL_ENDING);

			WSRequest request = play.libs.WS.url(sb.toString());
			HttpResponse resp = request.get();

			JsonElement json = new JsonParser().parse(resp.getString());

			JsonArray arr = json.getAsJsonObject().get("response")
					.getAsJsonArray();

			for (JsonElement el : arr) {
				String online = el.getAsJsonObject().get("online")
						.getAsString();
				String uid = el.getAsJsonObject().get("uid").getAsString();

				if (online.equals("1")) {
					lsHolder.addNew(uid, null);

					j += 1;
					System.out.println(uid + " added ----------------");
				} else if (online.equals("0")) {
					System.out.println(uid);
				} else {
					System.out.println(online + "-fuck");
					return;
				}
			}
		}
	}
}
