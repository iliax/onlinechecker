package jobs;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sun.org.apache.bcel.internal.generic.DMUL;

import dbstuff.DbLogger.LEVEL;

import static jobs.JobInvoker.*;

import play.Logger;
import play.jobs.Job;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import util.StaticDataHolder;
import static util.VkMotitorConstants.*;
import static util.StaticDataHolder.*;

class JobRunnable extends Job implements Runnable {

	public static volatile AtomicInteger count = new AtomicInteger(
			THREADS_COUNT);

	private List<String> uids;

	public JobRunnable(List<String> uids) {
		this.uids = uids;
	}

	@Override
	public void doJob() throws Exception {
		run();
	}

	@Override
	public void run() {
		count.decrementAndGet();

		try {
			for (int i = 0; i < uids.size();) {

				StringBuilder sb = new StringBuilder(URL_BEGINING);

				for (int j = 0; i < uids.size() && j < ITEMS_IN_ONE_REQUEST; i++, j++) {
					String t = uids.get(i);
					sb.append(t);
					sb.append(",");
				}
				sb.deleteCharAt(sb.length() - 1);
				sb.append(URL_ENDING);

				HttpResponse resp;

				WSRequest request = play.libs.WS.url(sb.toString());
				request.timeout(VK_REQUEST_TIMEOUT);
				long time = System.currentTimeMillis();
				resp = request.get();

				if (resp.getStatus() != 200) {
					Logger.warn("Bad response status  = " + resp.getStatus());
					dbLogger.log("Bad response status  = " + resp.getStatus(),
							LEVEL.WARN);

					// TODO is it right?
					continue;
				}

				JsonElement json = new JsonParser().parse(resp.getString());

				JsonArray arr = json.getAsJsonObject().get("response")
						.getAsJsonArray();
				for (JsonElement el : arr) {
					String uid = el.getAsJsonObject().get("uid").getAsString();
					String online = el.getAsJsonObject().get("online")
							.getAsString();
					saveNewRecord(uid, online, String.valueOf(time));
				}

			}

			//Logger.info("ended " + count + " at " + new Date());

		} catch (Exception e) {
			Logger.error(e, "Error in run() method");
			dbw.addCheckLogRecord("passed(err in run() method)");
			dbLogger.log("Error in run() method", e, LEVEL.ERROR);
		} finally {
			if (count.incrementAndGet() == THREADS_COUNT) {
				try {
					dbm.mongo.fsync(true);
				} catch (Exception e) {
					Logger.error(e, "Mongo fsync err");
					dbLogger.log("Mongo fsync error", e);
				}
			}

		}
	}

	private void saveNewRecord(String uid, String online, String time) {

		if (!checkStatusChanged(uid, online)) {
			return;
		}

		dbw.saveNewRecord(uid, online, time);
	}

	/** returns true - if status has been changed */
	private boolean checkStatusChanged(String uid, String online) {

		String oldStatus = lsHolder.getStatus(uid);
		if (oldStatus != null) {
			if (oldStatus.equals(online)) {
				return false;
			} else {
				lsHolder.updateStatus(uid, online);
			}
		}

		return true;
	}

}