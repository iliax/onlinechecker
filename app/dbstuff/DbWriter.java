package dbstuff;

import static util.StaticDataHolder.dbm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import com.mongodb.BasicDBObject;


public class DbWriter {

	private volatile DbManager dbm;
	
	public DbWriter(DbManager aDbm) {
		dbm = aDbm;
	}
	
	/** Логирует системные сообщения в отдельнут коллекцию */
	public synchronized void addCheckLogRecord(String status) {
		dbm.checksLog.save(new BasicDBObject("time", new Date().toGMTString())
				.append("status", status));
	}
	
	public synchronized void saveNewRecord(String uid, String online,
			String time) {
		Map<String, String> rec = new HashMap<String, String>();
		rec.put("uid", uid);
		rec.put("online", online);
		rec.put("time", time);

		dbm.records.save(new BasicDBObject(rec));

	}
}
