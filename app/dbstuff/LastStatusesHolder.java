package dbstuff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.Logger;
import play.Play;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import static util.StaticDataHolder.dbm;
import static util.VkMotitorConstants.*;

public class LastStatusesHolder {

	private volatile Map<String, String> cache = Collections
			.synchronizedMap(new HashMap<String, String>());

	private volatile DbManager dbm;

	public LastStatusesHolder(DbManager dbManager) {
		this.dbm = dbManager;
	}

	/** вызывать только для существующих записей! */
	public void updateStatus(String uid, String online) {
		BasicDBObject update = new BasicDBObject().append("$set",
				new BasicDBObject().append("online", online));

		dbm.lastStatuses.update(new BasicDBObject("uid", uid), update);

		cache.put(uid, online);
	}

	public void addNew(String uid, String online) {
		dbm.lastStatuses.save(new BasicDBObject().append("uid", uid).append(
				"online", online));

		cache.put(uid, online);
	}

	public String getStatus(String uid) {
		return cache.get(uid);
		//
		// DBObject res = dbm.lastStatuses.findOne(new BasicDBObject("uid",
		// uid));
		// if (res == null) {
		// return null;
		// }
		//
		// return res.get("online").toString();
	}

	public void initCache() {
		cache.clear();

		DBCursor dbc = dbm.lastStatuses.find();
		while (dbc.hasNext()) {
			DBObject o = dbc.next();
			cache.put(o.get("uid").toString(), o.get("online").toString());
		}
		dbc.close();
		dbc = null;
		System.gc();
	}

	public List<String> getTargetUids() {
		return new ArrayList<String>(cache.keySet());
	}

}
