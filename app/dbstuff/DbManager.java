package dbstuff;

import java.net.UnknownHostException;
import java.util.Date;

import util.ServerStatus;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

import static util.StaticDataHolder.dbm;
import static util.VkMotitorConstants.*;

public class DbManager {

	public final Mongo mongo;

	public final DB db;

	public final DBCollection records;

	public final DBCollection lastStatuses;

	public final DBCollection targets;

	public final DBCollection checksLog;

	public final DBCollection dbLog;
	
	public final DBCollection security;

	public final DBCollection serverStatuses;
	
	public DbManager() throws Exception {
		mongo = new Mongo(MONGO_URL_AND_PORT);
		db = mongo.getDB(DB_NAME);
		records = db.getCollection("records");
		lastStatuses = db.getCollection("last_statuses");
		checksLog = db.getCollection("checks_log");
		dbLog = db.getCollection("db_log");
		targets = db.getCollection("targets");
		security = db.getCollection("security");
		serverStatuses = db.getCollection("server_statuses");
	}

	public void setServerStatus(final ServerStatus status, String cause) {
		
		/**
		 * { status : 1/0, long_date : 100500, date : java.Date.toString(),
		 * cause : String }
		 */

		BasicDBObject dbo = new BasicDBObject().append("status", status.toString())
				.append("long_date", System.currentTimeMillis())
				.append("date", new Date().toGMTString())
				.append("cause", cause);
		dbm.serverStatuses.save(dbo);
		dbm.mongo.fsync(false);
	}
}
