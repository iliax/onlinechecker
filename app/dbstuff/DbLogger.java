package dbstuff;

import java.util.Arrays;
import java.util.Date;


import com.mongodb.BasicDBObject;
import static util.VkMotitorConstants.*;
public class DbLogger {

	public enum LEVEL {
		WARN, INFO, ERROR, DEBUG;
	}

	final DbManager dbManager;

	public DbLogger(DbManager dbm) {
		dbManager = dbm;
	}

	@SuppressWarnings("deprecation")
	public void log(String mess, Throwable e, LEVEL lvl) {
		dbManager.dbLog.save(new BasicDBObject()
				.append("mess", mess)
				.append("exception-mess", e.getMessage())
				.append("exception-trace",
						Arrays.toString(e.getStackTrace()))
				.append("level", lvl.toString())
				.append("time", new Date().toGMTString()));
	}
	
	public void log(String mess, Throwable e) {
		log(mess, e, LEVEL.ERROR);
	}

	@SuppressWarnings("deprecation")
	public void log(String mess, LEVEL lvl) {
		dbManager.dbLog.save(new BasicDBObject().append("mess", mess)
				.append("level", lvl.toString())
				.append("time", new Date().toGMTString()));
	}

}
